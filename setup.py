from setuptools import setup


setup(
    name='matrix-app',
    packages=['matrix_app'],
    include_package_data=True,
    zip_safe=False,
    description='Bender server (DAG sandbox controller)',
    version='2020.2',
    author='Data Engineers of UPK',
    author_email='upk_de@sberbank.ru',
    install_requires=[
        'flask',
        'flask_testing'
    ]
)
