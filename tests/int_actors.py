import unittest
from flask_testing import TestCase

from matrix_app.app import create_app


class TestActors(TestCase):

    def create_app(self):
        return create_app()

    # def setUp(self) -> None:
    #     # init db, for example
    #     pass

    def test_neo_morpheus(self):
        """
        Replica of Neo with Morpheus
        """
        r1 = self.client.get('/neo/meets/morpheus')
        # r2 = self.client.get('/neo/meets/neo1')
        self.assert200(r1)
        # self.assert200(r2)

    # def tearDown(self) -> None:
    #     pass  # close db sessions and so on


if __name__ == '__main__':
    unittest.main()
