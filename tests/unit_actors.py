import unittest
from matrix_app.actors import Neo, Morpheus, WhiteRabbit


class TestActors(unittest.TestCase):
    def setUp(self) -> None:
        self.neo = Neo()
        self.morpheus = Morpheus()
        self.white_rabbit = WhiteRabbit()

    def test_neo(self):
        """
        Test Neo
        """
        self.assertIsNotNone(self.neo.interact(self.morpheus))

    def test_morpheus_wr(self):
        """
        Test Morpheus
        """
        self.assertRegex(self.morpheus.interact(self.white_rabbit), 'strangers')

    def test_wr_neo(self):
        """
        Test WhiteRabbit
        """
        self.assertIsNotNone(self.white_rabbit.interact(self.neo))

    def tearDown(self) -> None:
        del self.neo
        del self.morpheus
        del self.white_rabbit


if __name__ == '__main__':
    unittest.main()
